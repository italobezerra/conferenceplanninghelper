package main.networkevent;

import main.session.Session;
import main.track.Track;

import java.sql.Time;
import java.time.Duration;
import java.time.LocalTime;

import static main.constants.NetworkEventConstants.EARLIER_NETWORK_EVENT;
import static main.constants.SessionConstants.AFTERNOON_SESSION_FINISHING;

public class NetworkEvent
{
    private Track track;

    public NetworkEvent(Track track)
    {
        this.track = track;
    }

    public Time getNetworkEventTime()
    {
        Time networkEventTime;

        Session afternoonSession = track.getAfternoonSession();
        Duration afternoonSessionLeftDuration = afternoonSession.getLeftDuration();

        LocalTime firstAvailableTimeOnSession = AFTERNOON_SESSION_FINISHING.toLocalTime().minus(afternoonSessionLeftDuration);

        boolean timeIsTooMuchEarlier = firstAvailableTimeOnSession.isBefore(EARLIER_NETWORK_EVENT.toLocalTime());

        if (timeIsTooMuchEarlier)
        {
            networkEventTime = EARLIER_NETWORK_EVENT;
        }
        else
        {
            networkEventTime = Time.valueOf(AFTERNOON_SESSION_FINISHING.toLocalTime().minus(afternoonSessionLeftDuration));
        }

        return networkEventTime;
    }
}
