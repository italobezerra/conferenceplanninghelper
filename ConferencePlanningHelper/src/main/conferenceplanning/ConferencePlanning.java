package main.conferenceplanning;

import main.talk.Talk;
import main.track.Track;

import java.util.ArrayList;
import java.util.List;

public class ConferencePlanning
{
    private List<Track> trackList;

    public ConferencePlanning(List<Track> trackList)
    {
        this.trackList = trackList;
    }

    public List<Track> getTrackList()
    {
        return trackList;
    }

    public List<Talk> getTalkList()
    {
        List<Talk> talkList = new ArrayList<>();

        for (Track conferencePlanningTrack : trackList
        )
        {
            List<Talk> currentTalkList = conferencePlanningTrack.getTalksList();
            talkList.addAll(currentTalkList);
        }

        return talkList;
    }
}
