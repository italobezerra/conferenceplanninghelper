package main.conferenceplanning;

import main.networkevent.NetworkEvent;
import main.talk.TalkAndTimePair;
import main.track.Track;
import main.session.Session;

import java.sql.Time;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static main.constants.NetworkEventConstants.NETWORK_EVENT;
import static main.constants.TrackConstants.HOUR_OF_AM_PM_PATTERN;
import static main.constants.TrackConstants.LUNCH;

public class ConferencePlanningPrinter
{
    private ConferencePlanning conferencePlanning;

    public ConferencePlanningPrinter(ConferencePlanning conferencePlanning)
    {
        this.conferencePlanning = conferencePlanning;
    }

    public void showPlanning()
    {
        List<Track> trackList = conferencePlanning.getTrackList();
        for (Track currentTrack : trackList)
        {
            showTrackPlanning(currentTrack);
        }
    }

    private void showTrackPlanning(Track currentTrack)
    {
        int currentTrackNumber = currentTrack.getNumber();

        System.out.println("Track " + currentTrackNumber + ":");

        Session morningSession = currentTrack.getMorningSession();
        showTalks(morningSession);
        showLunch(currentTrack);

        Session afternoonSession = currentTrack.getAfternoonSession();
        showTalks(afternoonSession);
        showNetworkEvent(currentTrack);

        System.out.println();
    }

    private void showLunch(Track currentTrack)
    {
        Time lunchTime = currentTrack.getLunchTime();
        String lunchTextToShow = getLunchTextToShow(lunchTime);

        System.out.println(lunchTextToShow);
    }

    private String getLunchTextToShow(Time lunchTime)
    {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(HOUR_OF_AM_PM_PATTERN);
        String lunchTextToShow = lunchTime.toLocalTime().format(dateTimeFormatter) + " " + LUNCH;

        return lunchTextToShow;
    }

    private void showNetworkEvent(Track track)
    {
        NetworkEvent networkEvent = track.getNetworkEvent();
        Time networkEventTime = networkEvent.getNetworkEventTime();
        String networkEventTextToShow = networkEventTextToShow(networkEventTime);

        System.out.println(networkEventTextToShow);
    }

    private String networkEventTextToShow(Time networkEventTime)
    {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(HOUR_OF_AM_PM_PATTERN);
        String networkEventTimeText = networkEventTime.toLocalTime().format(dateTimeFormatter);

        String networkEventTextToShow = networkEventTimeText + " " + NETWORK_EVENT;

        return networkEventTextToShow;
    }

    private void showTalks(Session currentSession)
    {
        List<TalkAndTimePair> talkAndTimePairList = currentSession.getTalkAndTimePairList();

        for (TalkAndTimePair currentTalkAndTimePair : talkAndTimePairList)
        {
            String talkAndTimeTextToShow = currentTalkAndTimePair.getTextToShow();
            System.out.println(talkAndTimeTextToShow);
        }
    }
}
