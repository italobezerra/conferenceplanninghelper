package main.conferenceplanning;

import main.talk.Talk;
import main.talk.exceptions.InvalidTalkLengthException;
import main.track.Track;

import java.util.ArrayList;
import java.util.List;

public class ConferencePlanningParser
{
    public ConferencePlanning getConferencePlanning(List<String> talkInputStringList) throws InvalidTalkLengthException
    {
        List<Talk> talksList = getTalksList(talkInputStringList);
        List<Track> tracksList = getTracksList(talksList);

        ConferencePlanning conferencePlanning = new ConferencePlanning(tracksList);

        return conferencePlanning;
    }

    private List<Talk> getTalksList(List<String> talkInputStringList) throws InvalidTalkLengthException
    {
        List<Talk> talkList = new ArrayList<>();

        for (String currentTalkInputString : talkInputStringList)
        {
            Talk returnedTalk = new Talk(currentTalkInputString);
            talkList.add(returnedTalk);
        }

        return talkList;
    }

    private List<Track> getTracksList(List<Talk> talkList)
    {
        List<Track> trackList = new ArrayList();
        List<Talk> auxTalkList = talkList;
        int trackListSize = 0;

        boolean allTalksWereAddedToTracks = auxTalkList.isEmpty();

        while (!allTalksWereAddedToTracks)
        {
            trackListSize++;

            Track trackToAdd = new Track(trackListSize);
            trackToAdd.addAsTalksAsPossible(auxTalkList);

            List<Talk> addedTalks = trackToAdd.getTalksList();
            auxTalkList.removeAll(addedTalks);

            allTalksWereAddedToTracks = auxTalkList.isEmpty();

            trackList.add(trackToAdd);
        }

        return trackList;
    }
}
