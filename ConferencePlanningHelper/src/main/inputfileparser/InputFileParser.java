package main.inputfileparser;


import main.conferenceplanning.ConferencePlanning;
import main.talk.Talk;
import main.talk.exceptions.InvalidTalkLengthException;
import main.track.Track;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class InputFileParser
{
    public List<String> getTalkInputStringList(File talkListFile) throws IOException
    {
        List<String> talkInputStringList = new ArrayList<>();

        FileReader fileReader = new FileReader(talkListFile);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        addAllNonEmptyLinesToInputList(talkInputStringList, bufferedReader);

        return talkInputStringList;
    }

    private void addAllNonEmptyLinesToInputList(List<String> talkInputStringList, BufferedReader bufferedReader) throws IOException
    {
        String currentFileLine = bufferedReader.readLine();
        boolean allLinesWereRead = currentFileLine.equals(null);

        while (!allLinesWereRead)
        {
            currentFileLine = currentFileLine.trim();

            if (!currentFileLine.isEmpty())
            {
                talkInputStringList.add(currentFileLine);
            }

            currentFileLine = bufferedReader.readLine();
            allLinesWereRead = currentFileLine == null;
        }
    }
}

