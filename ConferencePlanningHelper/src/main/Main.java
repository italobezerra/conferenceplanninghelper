package main;

import main.conferenceplanning.ConferencePlanning;
import main.conferenceplanning.ConferencePlanningPrinter;
import main.conferenceplanning.ConferencePlanningParser;
import main.inputfileparser.InputFileParser;
import main.talk.Talk;
import main.talk.exceptions.InvalidTalkLengthException;
import main.track.Track;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Main
{
    public static void main(String[] args)
    {
        String inputFilePath = args[0];

        File conferencePlanningInput = new File(inputFilePath);

        try
        {
            showPlanning(conferencePlanningInput);
        } catch (IOException e)
        {
            e.printStackTrace();
        } catch (InvalidTalkLengthException e)
        {
            e.printStackTrace();
        }
    }

    private static void showPlanning(File conferencePlanningInput) throws IOException, InvalidTalkLengthException
    {
        InputFileParser inputFileParser = new InputFileParser();

        List<String> talksInput = inputFileParser.getTalkInputStringList(conferencePlanningInput);
        ConferencePlanningParser conferencePlanningParser = new ConferencePlanningParser();

        ConferencePlanning conferencePlanning = conferencePlanningParser.getConferencePlanning(talksInput);
        ConferencePlanningPrinter conferencePlanningPrinter = new ConferencePlanningPrinter(conferencePlanning);

        conferencePlanningPrinter.showPlanning();
    }
}
