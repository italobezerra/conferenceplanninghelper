package main.track;

import main.networkevent.NetworkEvent;
import main.session.AfternoonSession;
import main.session.MorningSession;
import main.session.Session;
import main.talk.Talk;

import java.sql.Time;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static main.constants.SessionConstants.MORNING_SESSION_FINISHING;

public class Track
{
    private final Session morningSession;
    private final Session afternoonSession;
    private final int number;

    public Track(int trackNumber)
    {
        this.morningSession = new MorningSession();
        this.afternoonSession = new AfternoonSession();
        this.number = trackNumber;
    }

    public NetworkEvent getNetworkEvent()
    {
        NetworkEvent networkEvent = new NetworkEvent(this);

        return networkEvent;
    }

    public void addAsTalksAsPossible(List<Talk> talkList)
    {
        List<Talk> talkListToAdd = new ArrayList<>();
        talkListToAdd.addAll(talkList);

        morningSession.addAsTalksAsPossible(talkListToAdd);

        List<Talk> morningSessionTalks = morningSession.getTalksList();
        talkListToAdd.removeAll(morningSessionTalks);

        afternoonSession.addAsTalksAsPossible(talkListToAdd);
    }

    public List<Talk> getTalksList()
    {
        List<Talk> TalksList = new ArrayList<>();

        List<Talk> morningTalks = morningSession.getTalksList();
        List<Talk> afternoonTalks = afternoonSession.getTalksList();

        TalksList.addAll(morningTalks);
        TalksList.addAll(afternoonTalks);

        return TalksList;
    }

    public Session getMorningSession()
    {
        return morningSession;
    }

    public Session getAfternoonSession()
    {
        return afternoonSession;
    }

    public int getNumber()
    {
        return number;
    }

    public Duration getLengthAvailableForTalks()
    {
        Duration morningSessionDuration = morningSession.getLeftDuration();
        Duration afternoonSessionDuration = afternoonSession.getLeftDuration();
        Duration durationAvailableForTalks = morningSessionDuration.plus(afternoonSessionDuration);

        return durationAvailableForTalks;
    }

    public Time getLunchTime()
    {
        Time lunchTime = MORNING_SESSION_FINISHING;

        return lunchTime;
    }
}
