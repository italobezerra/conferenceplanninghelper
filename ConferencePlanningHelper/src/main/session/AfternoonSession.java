package main.session;

import static main.constants.SessionConstants.*;

public class AfternoonSession extends Session
{
    @Override
    protected void setBeginning()
    {
        this.beginning = AFTERNOON_SESSION_BEGINNING;
    }

    @Override
    protected void setSessionShift()
    {
        this.sessionShiftEnum = SessionShiftEnum.AFTERNOON;
    }

    @Override
    protected void setFinishing()
    {
        this.finishing = AFTERNOON_SESSION_FINISHING;
    }
}
