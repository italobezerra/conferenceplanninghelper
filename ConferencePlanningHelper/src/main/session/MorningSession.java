package main.session;

import static main.constants.SessionConstants.*;

public class MorningSession extends Session
{
    @Override
    protected void setBeginning()
    {
        this.beginning = MORNING_SESSION_BEGINNING;
    }

    @Override
    protected void setSessionShift()
    {
        this.sessionShiftEnum = SessionShiftEnum.MORNING;
    }

    @Override
    protected void setFinishing()
    {
        this.finishing = MORNING_SESSION_FINISHING;
    }
}
