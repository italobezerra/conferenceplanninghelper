package main.session;

import main.talk.Talk;
import main.talk.TalkAndTimePair;

import java.sql.Time;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public abstract class Session
{
    protected Time beginning;
    protected Time finishing;
    protected SessionShiftEnum sessionShiftEnum;
    private List<TalkAndTimePair> talkAndTimePairList;

    public Session()
    {
        talkAndTimePairList = new ArrayList<>();
        setPeriod();
        setSessionShift();
    }

    private void setPeriod()
    {
        setBeginning();
        setFinishing();
    }

    protected abstract void setFinishing();

    protected abstract void setBeginning();

    public boolean addTalkIfPossible(Talk talk)
    {
        boolean talkWillBeAdded = false;
        boolean talkWasAdded;
        TalkAndTimePair talkAndTimePairToAdd = null;

        if (isEmpty())
        {
            talkAndTimePairToAdd = new TalkAndTimePair(talk, beginning);
            talkWillBeAdded = true;
        }
        else if (!isFull())
        {
            Duration talkLength = talk.getLength();
            Time lastAvailableTime = getNextAvailableTime();
            int comparisionResult = getLeftDuration().compareTo(talkLength);

            if (comparisionResult >= 0)
            {
                talkAndTimePairToAdd = new TalkAndTimePair(talk, lastAvailableTime);
                talkWillBeAdded = true;
            }
        }

        if (talkWillBeAdded)
        {
            talkAndTimePairList.add(talkAndTimePairToAdd);
        }

        talkWasAdded = talkWillBeAdded;

        return talkWasAdded;
    }

    public Time getNextAvailableTime()
    {
        Time lastAvailableTime;

        if (!talkAndTimePairList.isEmpty())
        {
            int talkAndTimePairListSize = talkAndTimePairList.size();
            int lastAndTimePairListIndex = talkAndTimePairListSize - 1;

            TalkAndTimePair lastTalkAndTime = talkAndTimePairList.get(lastAndTimePairListIndex);
            Time lastTalkTime = lastTalkAndTime.getTimeOnSession();
            LocalTime lastTalkLocalTime = lastTalkTime.toLocalTime();

            Talk lastTalk = lastTalkAndTime.getTalk();
            Duration lastTalkLength = lastTalk.getLength();

            lastAvailableTime = Time.valueOf(lastTalkLocalTime.plus(lastTalkLength));
        }
        else
        {
            lastAvailableTime = getBeginning();
        }
        return lastAvailableTime;
    }

    public boolean isEmpty()
    {
        boolean isEmpty = talkAndTimePairList.isEmpty();

        return isEmpty;
    }

    public boolean isFull()
    {
        boolean isFull;

        Duration leftDuration = getLeftDuration();
        int durationComparisionResult = Duration.ZERO.compareTo(leftDuration);

        if (durationComparisionResult == 0)
        {
            isFull = true;
        }
        else
        {
            isFull = false;
        }

        return isFull;
    }

    public Duration getLeftDuration()
    {
        Duration leftDuration;

        Duration totalTalksDuration = getTotalTalksDuration();
        Duration sessionDuration = Duration.between(getBeginning().toLocalTime(), getFinishing().toLocalTime());

        leftDuration = sessionDuration.minus(totalTalksDuration);

        return leftDuration;
    }

    private Duration getTotalTalksDuration()
    {
        Duration totalTalksDuration = Duration.ofMinutes(0);

        for (TalkAndTimePair talkAndTimePair :
                talkAndTimePairList)
        {
            Talk currentTalk = talkAndTimePair.getTalk();

            totalTalksDuration = totalTalksDuration.plus(currentTalk.getLength());
        }

        return totalTalksDuration;
    }

    protected abstract void setSessionShift();

    public SessionShiftEnum getSessionShift()
    {
        return sessionShiftEnum;
    }

    public List<TalkAndTimePair> getTalkAndTimePairList()
    {
        return talkAndTimePairList;
    }

    public List<Talk> getTalksList()
    {
        List<Talk> talkList = new ArrayList<>();

        for (TalkAndTimePair currentTalkAndTimePair : talkAndTimePairList
        )
        {
            Talk currentTalkAndTimePairTalkPro = currentTalkAndTimePair.getTalk();
            talkList.add(currentTalkAndTimePairTalkPro);
        }

        return talkList;
    }

    public Time getBeginning()
    {
        return beginning;
    }

    public Time getFinishing()
    {
        return finishing;
    }

    public void addAsTalksAsPossible(List<Talk> talkList)
    {
        List<Talk> talkListToAdd = new ArrayList<>();
        talkListToAdd.addAll(talkList);

        boolean someTalkWasAdded;
        if (!isFull())
        {
            do
            {
                someTalkWasAdded = putTalkWithPossibleLengthOnList(talkListToAdd);

            } while (someTalkWasAdded);
        }
    }

    private boolean putTalkWithPossibleLengthOnList(List<Talk> talkListToAdd)
    {
        boolean talkWasAdded = false;

        if (!isFull())
        {
            Talk talkToInsert;
            talkToInsert = findTalkThatCanBeAdded(talkListToAdd);

            if (talkToInsert != null)
            {
                addTalkIfPossible(talkToInsert);
                talkListToAdd.remove(talkToInsert);
                talkWasAdded = true;
            }
        }
        return talkWasAdded;
    }

    private Talk findTalkThatCanBeAdded(List<Talk> auxTalkList)
    {
        Talk currentTalk = null;

        boolean talkToAddWasFound = false;
        int currentTalkIndex = 0;

        while (!talkToAddWasFound && currentTalkIndex < auxTalkList.size())
        {
            currentTalk = auxTalkList.get(currentTalkIndex);
            Duration currentSessionLeftDuration = getLeftDuration();
            Duration currentTalkLength = currentTalk.getLength();

            int comparisionResult = currentTalkLength.compareTo(currentSessionLeftDuration);

            if (comparisionResult <= 0)
            {
                talkToAddWasFound = true;
            }
            else
            {
                currentTalkIndex++;
            }
        }
        if (!talkToAddWasFound)
        {
            currentTalk = null;
        }

        return currentTalk;
    }
}
