package main.constants;

public final class TrackConstants
{
    public static final String HOUR_OF_AM_PM_PATTERN = "hh:mma";
    public static final String LUNCH = "Lunch";
}
