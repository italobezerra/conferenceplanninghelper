package main.constants;

import java.sql.Time;

public final class SessionConstants
{
    public static final Time MORNING_SESSION_BEGINNING = Time.valueOf("09:00:00");
    public static final Time MORNING_SESSION_FINISHING = Time.valueOf("12:00:00");

    public static final Time AFTERNOON_SESSION_BEGINNING = Time.valueOf("13:00:00");
    public static final Time AFTERNOON_SESSION_FINISHING = Time.valueOf("17:00:00");
}
