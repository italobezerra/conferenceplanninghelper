package main.constants;

import java.time.Duration;

public final class TalkParserConstants
{
    public static final String LIGHTNING_STRING = "lightning";
    public static final Duration LIGHTNING_DURATION = Duration.ofMinutes(5);
}
