package main.constants;

import java.sql.Time;

public final class NetworkEventConstants
{
    public static final String NETWORK_EVENT = "Networking Event";
    public static final Time EARLIER_NETWORK_EVENT = Time.valueOf("16:00:00");
    public static final Time LATTER_NETWORK_EVENT = Time.valueOf("17:00:00");
}
