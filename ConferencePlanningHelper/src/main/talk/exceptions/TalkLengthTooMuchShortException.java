package main.talk.exceptions;

public class TalkLengthTooMuchShortException extends InvalidTalkLengthException
{
    @Override
    public String getMessage()
    {
        String message = "talk  length can`t be shorter than 1 minute";

        return message;
    }
}