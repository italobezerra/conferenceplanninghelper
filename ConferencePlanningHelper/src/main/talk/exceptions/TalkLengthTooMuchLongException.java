package main.talk.exceptions;

public class TalkLengthTooMuchLongException extends InvalidTalkLengthException
{
    @Override
    public String getMessage()
    {
        String message = "talk  length can`t be longer than 60 minutes";

        return message;
    }
}
