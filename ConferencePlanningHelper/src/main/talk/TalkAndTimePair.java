package main.talk;

import java.sql.Time;
import java.time.format.DateTimeFormatter;

import static main.constants.TrackConstants.HOUR_OF_AM_PM_PATTERN;

public class TalkAndTimePair
{
    private final Talk talk;
    private final Time timeOnSession;

    public TalkAndTimePair(Talk talk, Time timeOnTrack)
    {
        this.talk = talk;
        this.timeOnSession = timeOnTrack;
    }

    public Talk getTalk()
    {
        return talk;
    }

    public Time getTimeOnSession()
    {
        return timeOnSession;
    }


    public String getTextToShow()
    {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(HOUR_OF_AM_PM_PATTERN);

        String talkTimeText = timeOnSession.toLocalTime().format(dateTimeFormatter);
        String talkInputText = talk.getTalkInputText();

        String talkTextToShow = talkTimeText + " " + talkInputText;

        return talkTextToShow;
    }
}
