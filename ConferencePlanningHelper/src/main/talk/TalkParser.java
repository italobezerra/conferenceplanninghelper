package main.talk;

import main.talk.exceptions.InvalidTalkLengthException;
import main.talk.exceptions.TalkLengthTooMuchLongException;
import main.talk.exceptions.TalkLengthTooMuchShortException;

import java.time.Duration;

import static main.constants.TalkParserConstants.LIGHTNING_DURATION;
import static main.constants.TalkParserConstants.LIGHTNING_STRING;

public class TalkParser
{
    private boolean isLightning(String talkTextInput)
    {
        boolean isLightning = talkTextInput.contains(LIGHTNING_STRING);

        return isLightning;
    }

    public String getTitle(String talkTextInput)
    {
        String talkTitle;
        boolean isLightning = isLightning(talkTextInput);

        if (isLightning)
        {
            talkTitle = talkTextInput.replace(LIGHTNING_STRING, "");
        }
        else
        {
            talkTitle = getNonLightningLength(talkTextInput);
        }

        return talkTitle;
    }

    public Duration getLength(String talkTextInput) throws InvalidTalkLengthException
    {
        Duration talkLength;
        boolean isLightning = isLightning(talkTextInput);

        if (isLightning)
        {
            talkLength = LIGHTNING_DURATION;
        }
        else
        {
            Long lengthAsLong = getMinutesNumberFromTalkTextInput(talkTextInput);
            talkLength = Duration.ofMinutes(lengthAsLong);

            int lengthValidationResult = isLengthValid(talkLength);

            if (lengthValidationResult > 0)
            {
                InvalidTalkLengthException invalidTalkLengthException = new TalkLengthTooMuchLongException();
                throw invalidTalkLengthException;
            }
            else if (lengthValidationResult < 0)
            {
                InvalidTalkLengthException invalidTalkLengthException = new TalkLengthTooMuchShortException();
                throw invalidTalkLengthException;
            }
        }

        return talkLength;
    }

    private int isLengthValid(Duration talkLength)
    {
        int comparisionResult;

        Duration minTalkLength = Duration.ofMinutes(1);
        Duration maxTalkLength = Duration.ofMinutes(60);

        boolean isLongerThanMaxLength = talkLength.compareTo(maxTalkLength) > 0;
        boolean isShorterThanMinLength = talkLength.compareTo(minTalkLength) < 0;

        if (isLongerThanMaxLength)
        {
            comparisionResult = 1;
        }
        else if (isShorterThanMinLength)
        {
            comparisionResult = -1;
        }
        else
        {
            comparisionResult = 0;
        }

        return comparisionResult;
    }

    private Long getMinutesNumberFromTalkTextInput(String talkTextInput)
    {
        String talkTitle = getNonLightningLength(talkTextInput);

        String lengthWithWordMin = talkTextInput.replace(talkTitle, "");
        String[] lengthSplittedByNumberAndWordMin = lengthWithWordMin.split("min");

        String lengthAsString = lengthSplittedByNumberAndWordMin[0];

        return Long.valueOf(lengthAsString);
    }

    private String getNonLightningLength(String talkTextInput)
    {
        String titleFromMinutesRegex = "\\d+min";

        String splittedStringArray[] = talkTextInput.split(titleFromMinutesRegex);
        String talkTitle = splittedStringArray[0];

        return talkTitle;
    }
}
