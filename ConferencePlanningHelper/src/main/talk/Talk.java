package main.talk;

import main.talk.exceptions.InvalidTalkLengthException;

import java.time.Duration;

public class Talk
{
    private final String talkInputText;
    private final String title;
    private final Duration length;

    public Talk(String talkInputText) throws InvalidTalkLengthException
    {
        this.talkInputText = talkInputText;
        TalkParser talkParser = new TalkParser();

        title = talkParser.getTitle(talkInputText).trim();
        length = talkParser.getLength(talkInputText);
    }

    public String getTitle()
    {
        return title;
    }

    public Duration getLength()
    {
        return length;
    }

    public String getTalkInputText()
    {
        return talkInputText;
    }
}
