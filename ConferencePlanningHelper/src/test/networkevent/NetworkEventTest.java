package test.networkevent;

import main.networkevent.NetworkEvent;
import main.session.AfternoonSession;
import main.session.Session;
import main.track.Track;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.Time;
import java.time.Duration;

import static main.constants.NetworkEventConstants.EARLIER_NETWORK_EVENT;
import static main.constants.NetworkEventConstants.LATTER_NETWORK_EVENT;
import static org.junit.jupiter.api.Assertions.assertEquals;

class NetworkEventTest
{
    @Test
    void shouldReturnEarlierNetworkEventTime()
    {
        Track fakeTrack = Mockito.mock(Track.class);

        Session fakeAfternoonSession = Mockito.mock(AfternoonSession.class);
        Mockito.when(fakeTrack.getAfternoonSession()).thenReturn(fakeAfternoonSession);

        Duration leftSessionDuration = Duration.ofHours(10);
        Mockito.when(fakeAfternoonSession.getLeftDuration()).thenReturn(leftSessionDuration);

        NetworkEvent networkEventSut = new NetworkEvent(fakeTrack);

        Time receivedTime = networkEventSut.getNetworkEventTime();

        assertEquals(EARLIER_NETWORK_EVENT, receivedTime);
    }

    @Test
    void shouldReturnLatterNetworkEventTime()
    {
        Track fakeTrack = Mockito.mock(Track.class);

        Session fakeAfternoonSession = Mockito.mock(AfternoonSession.class);
        Mockito.when(fakeTrack.getAfternoonSession()).thenReturn(fakeAfternoonSession);

        Mockito.when(fakeAfternoonSession.getLeftDuration()).thenReturn(Duration.ZERO);
        NetworkEvent networkEventSut = new NetworkEvent(fakeTrack);

        Time receivedTime = networkEventSut.getNetworkEventTime();

        assertEquals(LATTER_NETWORK_EVENT, receivedTime);
    }

    @Test
    void shouldReturnNetworkEventTimeOnRange()
    {
        Track fakeTrack = Mockito.mock(Track.class);

        Session fakeAfternoonSession = Mockito.mock(AfternoonSession.class);
        Mockito.when(fakeTrack.getAfternoonSession()).thenReturn(fakeAfternoonSession);

        Duration leftSessionDuration = Duration.ofMinutes(30);
        Mockito.when(fakeAfternoonSession.getLeftDuration()).thenReturn(leftSessionDuration);

        NetworkEvent networkEventSut = new NetworkEvent(fakeTrack);

        Time receivedTime = networkEventSut.getNetworkEventTime();
        boolean isTimeOnRange = !receivedTime.before(EARLIER_NETWORK_EVENT) && !receivedTime.after(LATTER_NETWORK_EVENT);

        assertEquals(true, isTimeOnRange);
    }
}