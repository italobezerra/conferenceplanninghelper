package test;

import main.talk.Talk;
import org.mockito.Mockito;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class MockHelper
{
    public static List<Talk> getOneHourTalkMockList(int listSize)
    {
        List<Talk> talkList = new ArrayList<>();

        int listSizeAux = listSize;

        while (listSizeAux > 0)
        {
            Talk talkMock = Mockito.mock(Talk.class);

            Mockito.when(talkMock.getTitle()).thenReturn("test title" + listSizeAux);
            Mockito.when(talkMock.getLength()).thenReturn(Duration.ofMinutes(60));
            Mockito.when(talkMock.getTalkInputText()).thenReturn("test title " + listSizeAux + "60min");

            talkList.add(talkMock);
            listSizeAux--;
        }

        return talkList;
    }

    public static Talk getTalkMockWithLength(int talkLengthMinutes)
    {
        Talk talkMock = Mockito.mock(Talk.class);
        Mockito.when(talkMock.getLength()).thenReturn(Duration.ofMinutes(talkLengthMinutes));

        return talkMock;
    }
}
