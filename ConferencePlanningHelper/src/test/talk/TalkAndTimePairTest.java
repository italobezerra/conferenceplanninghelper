package test.talk;

import main.talk.Talk;
import main.talk.TalkAndTimePair;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.Time;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TalkAndTimePairTest
{
    @Test
    void shouldReturnExpectedTalk()
    {
        Talk talkMock = Mockito.mock(Talk.class);
        Time time = Time.valueOf("14:00:00");

        TalkAndTimePair talkAndTimePairSutSut = new TalkAndTimePair(talkMock, time);
        Talk receivedTalk = talkAndTimePairSutSut.getTalk();

        assertEquals(talkMock, receivedTalk);
    }

    @Test
    void shouldReturnExpectedTimeOnSession()
    {
        Talk talkMock = Mockito.mock(Talk.class);
        Time time = Time.valueOf("14:00:00");

        TalkAndTimePair talkAndTimePairSut = new TalkAndTimePair(talkMock, time);
        Time receivedTime = talkAndTimePairSut.getTimeOnSession();

        assertEquals(time, receivedTime);
    }

    @Test
    void shouldReturnExpectedTimeTextToShow()
    {
        Talk talkMock = Mockito.mock(Talk.class);
        Time time = Time.valueOf("14:00:00");

        String talkTextInput = "Talk Test 30min";
        String expectedText = "02:00PM " + talkTextInput;

        Mockito.when(talkMock.getTalkInputText()).thenReturn(talkTextInput);

        TalkAndTimePair talkAndTimePairSut = new TalkAndTimePair(talkMock, time);
        String receivedText = talkAndTimePairSut.getTextToShow();

        assertEquals(expectedText, receivedText);
    }
}