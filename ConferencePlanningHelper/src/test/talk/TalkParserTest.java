package test.talk;

import main.talk.TalkParser;
import main.talk.exceptions.InvalidTalkLengthException;
import main.talk.exceptions.TalkLengthTooMuchLongException;
import main.talk.exceptions.TalkLengthTooMuchShortException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;

import java.time.Duration;

import static main.constants.TalkParserConstants.LIGHTNING_DURATION;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TalkParserTest
{
    @Test
    void shouldReturnExpectedTitle()
    {
        String expectedTalkTitle = "Talk title";
        String talkAsText = expectedTalkTitle + "50min";

        TalkParser talkParserSut = new TalkParser();
        String talkTitleSut = talkParserSut.getTitle(talkAsText);

        assertEquals(expectedTalkTitle, talkTitleSut);
    }

    @Test
    void shouldReturnExpectedLength() throws InvalidTalkLengthException
    {
        Duration expectedTalkLength = Duration.ofMinutes(50);
        String talkAsText = "Talk title 50min";

        TalkParser talkParserSut = new TalkParser();
        Duration testedTalkLength;

        testedTalkLength = talkParserSut.getLength(talkAsText);

        assertEquals(expectedTalkLength, testedTalkLength);
    }

    @Test
    void verifyTalkLengthWhenInputHasNumbers() throws InvalidTalkLengthException
    {
        String talkAsText = "Talk title 30min";
        TalkParser talkParserSut = new TalkParser();

        Duration talkParserSutLength;
        Duration expectedLength = Duration.ofMinutes(30);

        talkParserSutLength = talkParserSut.getLength(talkAsText);

        assertEquals(expectedLength, talkParserSutLength);
    }

    @Test
    void verifyTalkLengthWhenInputHasLightningTextAsLength() throws InvalidTalkLengthException
    {
        String talkAsText = "Talk title lightning";
        TalkParser talkParserSut = new TalkParser();

        Duration talkParsetSutLength;
        Duration expectedLength = LIGHTNING_DURATION;

        talkParsetSutLength = talkParserSut.getLength(talkAsText);

        assertEquals(expectedLength, talkParsetSutLength);
    }

    @Test
    void shouldReturnTalkTooMuchLongException()
    {
        String talkAsText = "Talk title 61min";
        TalkParser talkParserSut = new TalkParser();

        ThrowingSupplier getLengthThrowingSupplier = () -> talkParserSut.getLength(talkAsText);

        assertThrows(TalkLengthTooMuchLongException.class, getLengthThrowingSupplier);
    }

    @Test
    void shouldReturnTalkTooMuchShortException()
    {
        String talkAsText = "Talk title 0min";
        TalkParser talkParserSut = new TalkParser();

        ThrowingSupplier getLengthThrowingSupplier = () -> talkParserSut.getLength(talkAsText);

        assertThrows(TalkLengthTooMuchShortException.class, getLengthThrowingSupplier);
    }
}