package test.talk;

import main.constants.TalkParserConstants;
import main.talk.Talk;
import main.talk.exceptions.InvalidTalkLengthException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;

class TalkTest
{
    @Test
    void shouldReturnExpectedTitle() throws InvalidTalkLengthException
    {
        String expectedTitle = "Test title";
        String talkLengthInput = " 45min";
        Talk talkSut = new Talk(expectedTitle + talkLengthInput);

        String talkSutTitle = talkSut.getTitle();

        Assertions.assertEquals(expectedTitle, talkSutTitle);
    }

    @Test
    void showReturnExpectedLength() throws InvalidTalkLengthException
    {
        Duration expectedTalkLength = Duration.ofMinutes(45);

        String talkInput = "Test title 45min";
        Talk talkSut = new Talk(talkInput);
        Duration talkSutLength = talkSut.getLength();

        Assertions.assertEquals(expectedTalkLength, talkSutLength);
    }

    @Test
    void shouldReturnExpectedTextInput() throws InvalidTalkLengthException
    {
        String expectedTalkInput = "Test title 45min";
        Talk talkSut = new Talk(expectedTalkInput);
        String talkSutTextInput = talkSut.getTalkInputText();

        Assertions.assertEquals(expectedTalkInput, talkSutTextInput);
    }

    @Test
    void verifyTalkLengthWhenInputHasLightningTextAsLength() throws InvalidTalkLengthException
    {
        String talkAsText = "Talk title lightning";
        Talk talkSut = new Talk(talkAsText);

        Duration talkSutLength = talkSut.getLength();
        Duration expectedLength = TalkParserConstants.LIGHTNING_DURATION;

        Assertions.assertEquals(expectedLength, talkSutLength);
    }
}