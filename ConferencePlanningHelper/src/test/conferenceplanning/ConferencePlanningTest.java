package test.conferenceplanning;

import main.conferenceplanning.ConferencePlanning;
import main.talk.Talk;
import main.track.Track;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConferencePlanningTest
{
    @Test
    void shouldReturnExpectedTalksListWhenHasTwoTracks()
    {
        Track trackFake1 = Mockito.mock(Track.class);
        Track trackFake2 = Mockito.mock(Track.class);

        Talk talkFake1 = Mockito.mock(Talk.class);
        Talk talkFake2 = Mockito.mock(Talk.class);

        List<Talk> talkListFake1 = new ArrayList<>();
        talkListFake1.add(talkFake1);

        List<Talk> talkListFake2 = new ArrayList<>();
        talkListFake2.add(talkFake2);

        List<Talk> talkListExpected = new ArrayList<>();
        talkListExpected.add(talkFake1);
        talkListExpected.add(talkFake2);

        Mockito.when(trackFake1.getTalksList()).thenReturn(talkListFake1);
        Mockito.when(trackFake2.getTalksList()).thenReturn(talkListFake2);

        List trackList = new ArrayList();
        trackList.add(trackFake1);
        trackList.add(trackFake2);

        ConferencePlanning conferencePlanningSut = new ConferencePlanning(trackList);
        List<Talk> conferencePlanningSutTalkList = conferencePlanningSut.getTalkList();

        assertEquals(talkListExpected, conferencePlanningSutTalkList);
    }
}
