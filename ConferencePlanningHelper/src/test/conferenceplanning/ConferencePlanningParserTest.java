package test.conferenceplanning;

import main.conferenceplanning.ConferencePlanning;
import main.conferenceplanning.ConferencePlanningParser;

import main.talk.Talk;
import main.talk.exceptions.InvalidTalkLengthException;
import main.track.Track;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ConferencePlanningParserTest
{
    @Test
    void shouldReturnANotNullConferencePlanningInstance() throws InvalidTalkLengthException
    {
        ConferencePlanningParser conferencePlanningParserSut = new ConferencePlanningParser();
        ConferencePlanning receivedConferencePlanning;

        List<String> stringListFake = new ArrayList<>();
        receivedConferencePlanning = conferencePlanningParserSut.getConferencePlanning(stringListFake);

        assertNotNull(receivedConferencePlanning);
    }

    @Test
    void shouldReturnConferencePlanningInstanceWith2Talks() throws InvalidTalkLengthException
    {
        ConferencePlanningParser conferencePlanningParserSut = new ConferencePlanningParser();
        ConferencePlanning receivedConferencePlanning;

        String stringFake1 = "test talk 30min";
        String stringFake2 = "test talk 60min";

        List<String> stringListFake = new ArrayList<>();
        stringListFake.add(stringFake1);
        stringListFake.add(stringFake2);

        receivedConferencePlanning = conferencePlanningParserSut.getConferencePlanning(stringListFake);

        List<Talk> talkList = receivedConferencePlanning.getTalkList();
        int talkListReturnedSize = talkList.size();

        assertEquals(2,talkListReturnedSize);
    }
}