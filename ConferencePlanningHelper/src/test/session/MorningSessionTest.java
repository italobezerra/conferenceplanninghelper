package test.session;

import main.session.AfternoonSession;
import main.session.MorningSession;
import main.session.Session;
import main.session.SessionShiftEnum;
import main.talk.Talk;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.Time;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static main.constants.SessionConstants.*;
import static main.session.SessionShiftEnum.AFTERNOON;
import static main.session.SessionShiftEnum.MORNING;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static test.MockHelper.getOneHourTalkMockList;
import static test.MockHelper.getTalkMockWithLength;

class MorningSessionTest
{
    @Test
    void shouldReturnMorningSessionShift()
    {
        Session sessionSut = new MorningSession();
        SessionShiftEnum sessionSutSessionShift = sessionSut.getSessionShift();
        SessionShiftEnum expectedSessionShit = MORNING;

        assertEquals(sessionSutSessionShift, expectedSessionShit);
    }

   @Test
    void shouldReturnMorningSessionBeginning()
    {
        Session morningSessionSut = new MorningSession();
        Time morningSessionSutBeginning = morningSessionSut.getBeginning();
        Time morningSessionExpectedBeginning = MORNING_SESSION_BEGINNING;

        assertEquals(morningSessionSutBeginning, morningSessionExpectedBeginning);
    }

    @Test
    void shouldReturnMorningSessionFinishing()
    {
        Session morningSessionSut = new MorningSession();
        Time morningSessionSutFinishing = morningSessionSut.getFinishing();
        Time morningSessionExpectedFinishing = MORNING_SESSION_FINISHING;

        assertEquals(morningSessionSutFinishing, morningSessionExpectedFinishing);
    }

   }