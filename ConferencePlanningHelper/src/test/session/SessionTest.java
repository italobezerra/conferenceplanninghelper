package test.session;

import main.session.AfternoonSession;
import main.session.MorningSession;
import main.session.Session;
import main.session.SessionShiftEnum;
import main.talk.Talk;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.Time;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static main.constants.SessionConstants.*;
import static main.session.SessionShiftEnum.AFTERNOON;
import static main.session.SessionShiftEnum.MORNING;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static test.MockHelper.getOneHourTalkMockList;
import static test.MockHelper.getTalkMockWithLength;

class SessionTest
{
    @Test
    void mustVerifyTwoTalksWereAdded()
    {
        Session sessionSut = new MorningSession();
        int talkLengthMinutes = 40;

        Talk talkMock = getTalkMockWithLength(talkLengthMinutes);

        sessionSut.addTalkIfPossible(talkMock);
        sessionSut.addTalkIfPossible(talkMock);

        int talkListSize = sessionSut.getTalkAndTimePairList().size();

        assertEquals(talkListSize, 2);
    }

    @Test
    void shouldReturnIsFullTrue()
    {
        Session sessionSut = new MorningSession();
        int talkLengthMinutes = 60;

        Talk talkMock = getTalkMockWithLength(talkLengthMinutes);
        sessionSut.addTalkIfPossible(talkMock);
        sessionSut.addTalkIfPossible(talkMock);
        sessionSut.addTalkIfPossible(talkMock);

        boolean methodResponse = sessionSut.isFull();

        assertEquals(true, methodResponse);
    }

    @Test
    void shouldReturnIsFullFalse()
    {
        Session sessionSut = new MorningSession();

        int talkLengthMinutes = 50;
        Talk talk = getTalkMockWithLength(talkLengthMinutes);
        sessionSut.addTalkIfPossible(talk);

        boolean methodResponse = sessionSut.isFull();

        assertEquals(false, methodResponse);
    }

    @Test
    void shouldReturnIsEmptyTrue()
    {
        Session sessionSut = new MorningSession();
        boolean isSessionSutEmpty = sessionSut.isEmpty();

        assertEquals(true, isSessionSutEmpty);
    }

    @Test
    void shouldReturnIsEmptyFalse()
    {
        Session sessionSut = new MorningSession();
        Talk talkMock = Mockito.mock(Talk.class);

        sessionSut.addTalkIfPossible(talkMock);
        boolean isSessionSutEmpty = sessionSut.isEmpty();

        assertEquals(false, isSessionSutEmpty);
    }

    @Test
    void getLeftDuration()
    {
        Session sessionSut = new MorningSession();
        Duration expectedDuration = Duration.ofMinutes(160);

        int talkLengthMinutes = 20;
        Talk talk = getTalkMockWithLength(talkLengthMinutes);
        sessionSut.addTalkIfPossible(talk);

        Duration sessionSutLeftDuration = sessionSut.getLeftDuration();

        assertEquals(expectedDuration, sessionSutLeftDuration);
    }

     @Test
    void shouldReturnNextAvailableTimeWhenSessionIsNotEmpty()
    {
        Time lastAvailableTimeReturned;
        Time expectedTime = Time.valueOf("09:45:00");

        int talkLengthMinutes = 45;
        Talk talk = getTalkMockWithLength(talkLengthMinutes);
        Session morningSessionSut = new MorningSession();
        morningSessionSut.addTalkIfPossible(talk);

        lastAvailableTimeReturned = morningSessionSut.getNextAvailableTime();

        assertEquals(expectedTime, lastAvailableTimeReturned);
    }

    @Test
    void shouldReturnNextAvailableTimeWhenSessionIsEmpty()
    {
        Time lastAvailableTimeReturned;
        Time expectedTime = MORNING_SESSION_BEGINNING;

        Session morningSessionSut = new MorningSession();
        lastAvailableTimeReturned = morningSessionSut.getNextAvailableTime();

        assertEquals(expectedTime, lastAvailableTimeReturned);
    }

    @Test
    void shouldVerifyTalkListAdditionWhenListSmallerThanSession()
    {
        Session morningSessionSut = new MorningSession();
        List<Talk> talkList = new ArrayList<>();

        talkList.add(getTalkMockWithLength(40));
        talkList.add(getTalkMockWithLength(60));

        morningSessionSut.addAsTalksAsPossible(talkList);

        List<Talk> receivedTalkList = morningSessionSut.getTalksList();

        assertEquals(talkList, receivedTalkList);
    }

    @Test
    void shouldVerifyTalkListAdditionWhenListBiggerThanSession()
    {
        Session morningSessionSut = new MorningSession();

        int listSize = 12;
        List<Talk> talkList = getOneHourTalkMockList(listSize);

        Duration morningSessionSutLeftDuration = morningSessionSut.getLeftDuration();
        long hoursAvailableForTalks = morningSessionSutLeftDuration.toHours();

        morningSessionSut.addAsTalksAsPossible(talkList);

        List<Talk> receivedList = morningSessionSut.getTalksList();
        long receivedTalkListSize = receivedList.size();

        assertEquals(hoursAvailableForTalks, receivedTalkListSize);
    }

}