package test.session;

import main.session.AfternoonSession;
import main.session.MorningSession;
import main.session.Session;
import main.session.SessionShiftEnum;
import main.talk.Talk;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.Time;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static main.constants.SessionConstants.*;
import static main.session.SessionShiftEnum.AFTERNOON;
import static main.session.SessionShiftEnum.MORNING;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static test.MockHelper.getOneHourTalkMockList;
import static test.MockHelper.getTalkMockWithLength;

class AfternoonSessionTest
{
    @Test
    void shouldReturnAfternoonSessionShift()
    {
        Session sessionSut = new AfternoonSession();
        SessionShiftEnum sessionSutSessionShift = sessionSut.getSessionShift();
        SessionShiftEnum expectedSessionShit = AFTERNOON;

        assertEquals(sessionSutSessionShift, expectedSessionShit);
    }

    @Test
    void shouldReturnAfternoonSessionBeginning()
    {
        Session afternoonSessionSut = new AfternoonSession();
        Time afternoonSessionSutBeginning = afternoonSessionSut.getBeginning();
        Time afternoonSessionExpectedBeginning = AFTERNOON_SESSION_BEGINNING;

        assertEquals(afternoonSessionSutBeginning, afternoonSessionExpectedBeginning);
    }

    @Test
    void shouldReturnAfternoonSessionFinishing()
    {
        Session afternoonSessionSut = new AfternoonSession();
        Time afternoonSessionSutFinishing = afternoonSessionSut.getFinishing();
        Time afternoonSessionExpectedFinishing = AFTERNOON_SESSION_FINISHING;

        assertEquals(afternoonSessionSutFinishing, afternoonSessionExpectedFinishing);
    }

}