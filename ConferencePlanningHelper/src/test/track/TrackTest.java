package test.track;

import main.networkevent.NetworkEvent;
import main.talk.Talk;
import main.track.Track;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static test.MockHelper.getOneHourTalkMockList;

class TrackTest
{
    @Test
    void shouldReturnAddedTalksWhenAllCanBeAdded()
    {
        int listSize = 4;
        List<Talk> talkList = getOneHourTalkMockList(listSize);
        Track trackSut = new Track(1);
        trackSut.addAsTalksAsPossible(talkList);

        List<Talk> trackSutTalksList = trackSut.getTalksList();

        assertEquals(talkList, trackSutTalksList);
    }

    @Test
    void verifyAddedTalksWhenSomeTalksCouldNotBeAdded()
    {
        int listSize = 20;

        List<Talk> talkListFake = getOneHourTalkMockList(listSize);
        Track trackSut = new Track(1);

        long hoursAvailableForTalks = trackSut.getLengthAvailableForTalks().toHours();
        trackSut.addAsTalksAsPossible(talkListFake);

        List<Talk> trackSutTalksList = trackSut.getTalksList();
        long receivedTalkListSize = trackSutTalksList.size();

        assertEquals(hoursAvailableForTalks, receivedTalkListSize);
    }

    @Test
    void shouldReturnExpectedNumber()
    {
        int expectedTrackNumber = 1;

        Track trackSut = new Track(expectedTrackNumber);
        int trackSutNumber = trackSut.getNumber();

        assertEquals(expectedTrackNumber, trackSutNumber);
    }

    @Test
    void shouldReturnNetworkEventNotNullInstance()
    {
        Track trackSut = new Track(1);
        NetworkEvent trackSutNetworkEvent = trackSut.getNetworkEvent();

        Assertions.assertNotNull(trackSutNetworkEvent);
    }

}