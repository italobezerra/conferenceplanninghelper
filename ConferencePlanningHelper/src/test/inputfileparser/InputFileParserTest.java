package test.inputfileparser;


import main.inputfileparser.InputFileParser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class InputFileParserTest
{
    @Test
    void shouldReturnAllStringTalksFromInputFile() throws IOException
    {
        int expectedLength = 19;

        InputFileParser inputFileParserSut = new InputFileParser();
        File file = new File("talksListInput.txt");

        List<String> inputFileParserSutTalkInputList = inputFileParserSut.getTalkInputStringList(file);
        int sutTalkInputListLength = inputFileParserSutTalkInputList.size();

        assertEquals(expectedLength, sutTalkInputListLength);
    }

    @Test
    void shouldReturnIOException()
    {
        File file = new File("non existing path");

        InputFileParser inputFileParserSut = new InputFileParser();
        ThrowingSupplier getConferencePlanningThrowingSupplier = () -> inputFileParserSut.getTalkInputStringList(file);

        assertThrows(IOException.class, getConferencePlanningThrowingSupplier);
    }
}

